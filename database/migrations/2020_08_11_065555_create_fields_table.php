<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fields', function (Blueprint $table): void {
            $table->id();
            $table->string('type')->nullable();
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->json('values')->nullable();
            $table->text('default_value')->nullable();
            $table->text('placeholder')->nullable();
            $table->boolean('required')->default(false);
            $table->unsignedInteger('order_column')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('field_modals', function (Blueprint $table): void {
            $table->foreignId('field_id')->constrained('fields');
            $table->morphs('modal');
        });

        Schema::create('fields_responses', function (Blueprint $table): void {
            $table->id();
            $table->foreignId('field_id')->constrained('fields');
            $table->morphs('modal');
            $table->json('value');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fields_responses');
        Schema::dropIfExists('field_modals');
        Schema::dropIfExists('fields');
    }
}

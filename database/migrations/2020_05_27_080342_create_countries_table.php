<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->id();
            $table->string('name')->nullable();
            $table->string('capital')->nullable();
            $table->string('iso_alpha2', 2)->nullable();
            $table->string('iso_alpha3', 3)->nullable();
            $table->string('iso_numeric', 3)->nullable();
            $table->string('address_format')->nullable();
            $table->boolean('is_eu_member')->nullable();
            $table->boolean('uses_postal_code')->nullable();
            $table->json('calling_code')->nullable();
            $table->string('emoji')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}

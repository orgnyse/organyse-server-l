<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('questions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('organisation_id')->nullable()->constrained('organisations')->onDelete('cascade');
            $table->foreignId('question_type_id')->constrained('question_types');
            $table->text('title')->nullable();
            $table->text('description')->nullable();
            $table->json('values')->nullable();
            $table->text('default_value')->nullable();
			$table->text('placeholder')->nullable();
            $table->boolean('required')->default(false);
			$table->boolean('required_default')->default(false);
            $table->boolean('system_default')->default(false);
            $table->unsignedInteger('order_column')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('questions');
    }
}

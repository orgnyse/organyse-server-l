<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrganisationOrganiserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('organisation_organiser', function (Blueprint $table) {
            $table->id();
			$table->foreignId('organiser_id')->constrained('organisers')->onDelete('cascade');
			$table->foreignId('organisation_id')->constrained('organisations')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('organisation_organiser');
    }
}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('events', function (Blueprint $table) {
            $table->id();
            $table->foreignId('organisation_id')->constrained('organisations')->onDelete('cascade');
            $table->foreignId('event_type_id')->constrained('event_types');
            $table->foreignId('venue_id')->nullable()->constrained('venues')->onDelete('cascade');
            $table->string('title')->nullable();
            $table->string('slug')->nullable();
            $table->text('summary')->nullable();
            $table->longText('content')->nullable();
            $table->dateTime('start_date')->nullable()->index();
            $table->dateTime('end_date')->nullable()->index();
            $table->string('timezone')->nullable();
            $table->nestedSet();
            $table->boolean('is_featured')->default(false);
            $table->boolean('is_public')->default(false);
            $table->boolean('is_published')->default(false);
            $table->boolean('is_archived')->default(false);
            $table->boolean('social_share')->default(false);
            $table->text('before_order_text')->nullable();
            $table->text('after_order_text')->nullable();
            $table->text('offline_payment_text')->nullable();
            $table->unsignedInteger('order_column')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('events');
    }
}

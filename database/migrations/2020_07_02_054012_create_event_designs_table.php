<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEventDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('event_designs', function (Blueprint $table) {
            $table->id();
            $table->foreignId('event_id')->constrained('events')->onDelete('cascade');
			$table->boolean('show_email')->default(false);
			$table->string('email')->nullable();
			$table->boolean('show_phone')->default(false);
			$table->string('phone')->nullable();
            $table->string('bg_color')->nullable();
            $table->string('ticket_section_name')->nullable();
            $table->string('ticket_section_desc')->nullable();
            $table->string('reg_button_text')->nullable();
            $table->string('checkout_button_text')->nullable();
            $table->string('offline_method_text')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('event_designs');
    }
}

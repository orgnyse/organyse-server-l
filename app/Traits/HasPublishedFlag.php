<?php

namespace App\Traits;

use App\Scopes\PublishedFlagScope;

trait HasPublishedFlag
{
    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootHasPublishedFlag()
    {
        static::addGlobalScope(new PublishedFlagScope);
    }

}


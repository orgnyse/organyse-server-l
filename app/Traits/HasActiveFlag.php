<?php

namespace App\Traits;

use App\Scopes\ActiveFlagScope;

trait HasActiveFlag
{

    /**
     * Boot the soft deleting trait for a model.
     *
     * @return void
     */
    public static function bootHasActiveFlag()
    {
        static::addGlobalScope(new ActiveFlagScope);
    }
}

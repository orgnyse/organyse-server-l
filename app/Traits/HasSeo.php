<?php

namespace App\Traits;

use Illuminate\Database\Eloquent\Relations\MorphOne;

trait HasSeo
{
    /**
     * Boot the deleting trait for a model.
     *
     * @return void
     */
    public static function bootHasSeo(): void
    {
        static::deleting(fn ($item) => $item->seo()->delete());
    }

    /**
     * Get the model's seo data.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function seo(): MorphOne
    {
        return $this->morphOne(
            'App\Models\Seo', 
            'seoable'
        )->withDefault();
    }
}


<?php

namespace App\Traits;

use App\Scopes\OrganisationScope;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

trait HasOrganisation
{
    /**
     * Boot the organisation trait for a model.
     *
     * @return void
     */
    public static function bootHasOrganisation()
    {
        static::addGlobalScope(new OrganisationScope);

        static::creating(function ($model) {
            $model->organisation_id = Auth::guard('api-organiser')->user()->current_organisation_id;
        });

        static::updating(function ($model) {
            $model->organisation_id = Auth::guard('api-organiser')->user()->current_organisation_id;
        });
    }

    /**
     * Get the organisation the event category belongs to.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function organisation(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Organisation',
            'organisation_id',
            'id'
        );
    }
}

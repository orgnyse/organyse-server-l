<?php

namespace App\Http\Resources;

use Spatie\MediaLibrary\Conversions\Conversion;
use Illuminate\Http\Resources\Json\JsonResource;
use Spatie\MediaLibrary\Conversions\ConversionCollection;

class Media extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
			'name' => $this->name,
            'file' => $this->file_name,
			'mime' => $this->mime_type,
			'collection' => $this->collection_name,
			'urls' => $this->getVersions(),
			'order' => $this->order_column
        ];
    }

    /**
     * @return mixed
     */
    protected function getVersions(): array
    {
        $conversions = ConversionCollection::createForMedia($this->resource);

        $versions = $conversions->reduce(function ($carry, Conversion $conversion) {
            
			if ($conversion->shouldBePerformedOn($this->resource->collection_name)) {
                $name = $conversion->getName();
                $carry[$name] = $this->resource->getFullUrl($name);
            }

            return $carry;
        }, []);

        return array_merge($versions, [
            'original' => $this->resource->getFullUrl()
        ]);
    }
}

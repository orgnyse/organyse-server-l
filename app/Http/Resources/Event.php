<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Event extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id' => $this->id,
            'type' => new EventType($this->whenLoaded('type')),
            'title' => $this->title,
            'slug' => $this->slug,
            'summary' => $this->summary,
            'content' => $this->content,
            'start_date' => $this->start_date,
            'end_date' => $this->end_date,
            'timezone' => $this->timezone,
            'is_featured' => $this->is_featured,
            'is_public' => $this->is_public,
            'is_published' => $this->is_published,
            'enable_social_share' => $this->social_share,
            'venue' => new Venue($this->whenLoaded('venue')),
            'tickets' => Ticket::collection($this->whenLoaded('tickets')),
            'media' => Media::collection($this->whenLoaded('media')),
        ];
    }
}

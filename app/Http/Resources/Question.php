<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Question extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => new QuestionType($this->whenLoaded('type')),
			'title' => $this->title,
			'description' => $this->description,
			'values' => $this->values,
            'default' => $this->default_value,
            'placeholder' => $this->placeholder,
            'required' =>  $this->required,
		];
    }
}

<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Country extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'capital' => $this->capital,
            'iso_alpha2' => $this->iso_alpha2,
            'iso_alpha3' => $this->iso_alpha3,
            'is_eu_member' => $this->is_eu_member,
            'uses_postal_code' => $this->uses_postal_code,
            'calling_codes' => json_decode($this->calling_code, true),
            'emoji' => $this->emoji,
            'currencies' => Currency::collection($this->whenLoaded('currencies')),
        ];
    }
}

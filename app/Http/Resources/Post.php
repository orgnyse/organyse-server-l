<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Post extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
			'id' => $this->id,
            'user_id' => $this->user_id,
			'title' => $this->title,
			'slug' => $this->slug,
			'summary' => $this->summary,
            'content' => $this->content,
            'categories' => PostCategory::collection($this->whenLoaded('categories')),
            'author' =>  (new User($this->whenLoaded('author'))),
           // 'media' => Media::collection($this->whenLoaded('media')),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
		];
    }
}

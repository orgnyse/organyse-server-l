<?php

namespace App\Http\Controllers\Core;

use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;

class TimezoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $list = [];

        $list['General']['GMT'] = 'GMT timezone';
        $list['General']['UTC'] = 'UTC timezone';

        $continents = [
            'Africa'     => DateTimeZone::AFRICA,
            'America'    => DateTimeZone::AMERICA,
            'Antarctica' => DateTimeZone::ANTARCTICA,
            'Arctic'     => DateTimeZone::ARCTIC,
            'Asia'       => DateTimeZone::ASIA,
            'Atlantic'   => DateTimeZone::ATLANTIC,
            'Australia'  => DateTimeZone::AUSTRALIA,
            'Europe'     => DateTimeZone::EUROPE,
            'Indian'     => DateTimeZone::INDIAN,
            'Pacific'    => DateTimeZone::PACIFIC
        ];

        foreach ($continents as $continent => $mask) {
            $timezones = DateTimeZone::listIdentifiers($mask);

            foreach ($timezones as $timezone) {

                $time   = new DateTime(null, new DateTimeZone($timezone));
                
                $offset = $time->format('P');

                $tz = substr($timezone, strlen($continent) + 1);
                $tz = str_replace('St_', 'St. ', $tz);
                $tz = str_replace('_', ' ', $tz);

                $list[$continent][$timezone] = '(GMT/UTC' . $offset . ')' . ' ' . $tz;
            }
        }

        return new JsonResponse([
            'data' => $list
        ], 200);

    }
    
}

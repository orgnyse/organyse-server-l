<?php

namespace App\Http\Controllers\Core;

use App\Models\Country;
use App\Models\Division;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;
use App\Http\Resources\Country as CountryResource;
use App\Http\Resources\Division as DivisionResource;

class CountryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countries = Country::orderBy('name')->with([
            'currencies'
        ])->get();
        
        return CountryResource::collection($countries);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function country(Country $country)
    {
        $country->loadMissing([
            'currencies'
        ]);

        return new CountryResource($country);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Country  $country
     * @return \Illuminate\Http\Response
     */
    public function division(Country $country)
    {
        $divisions = $country->states()
            ->orderBy('name')
            ->get();
            
        return DivisionResource::collection($divisions);
    }

}

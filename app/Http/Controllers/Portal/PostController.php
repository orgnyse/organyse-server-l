<?php

namespace App\Http\Controllers\Portal;

use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\PostCategory as Category;
use App\Http\Resources\Post as PostResource;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with([
			'categories',
			'media',
			'author',
			'author.media'
		])->paginate();

        return PostResource::collection($posts);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function post(Post $post)
    {
        $post->loadMissing([
			'categories',
			'media',
			'author',
			'author.media'
        ]);
        
        return new PostResource($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PostCategory  $category
     * @return \Illuminate\Http\Response
     */
    public function category(Category $category)
    {
        $posts = $category->posts()
			->with([
				'categories', 
				'media', 
				'author', 
				'author.media'
			])->paginate(5);

        return PostResource::collection($posts);
    }
}

<?php

namespace App\Http\Controllers\Portal;

use App\Models\Event;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Event as EventResource;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::with([
            'media'
        ])->ignoreOrganisation()->paginate();

        return EventResource::collection($events);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $event)
    {
        $event = Event::wih([
            'media'
        ])->ignoreOrganisation()->where('id', $event)
            ->firstOrFail();

        return new EventResource($event);
    }

}

<?php

namespace App\Http\Controllers\Portal;

use Illuminate\Http\Request;
use App\Mail\Admin\ContactRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * handle the contact form response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function handleResponse(Request $request)
    {
        $formData = $request->validate([
			'firstname' => ['required', 'string'],
			'lastname' => ['required', 'string'],
            'email' => ['required', 'string', 'email'],
            'phone' => ['required', 'string'],
			'message' => ['required'],
        ]);
		
		Mail::send(new ContactRequest($formData));
		
		return response()->json([
            'message' => 'Mail sent'
        ], 200);
    }

}

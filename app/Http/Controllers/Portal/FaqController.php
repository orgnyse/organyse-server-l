<?php

namespace App\Http\Controllers\Portal;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\Faq as FaqResource;

class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return FaqResource::collection(Faq::paginate());
    }

}

<?php

namespace App\Http\Controllers\Portal;

use App\Models\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ArticleCategory as Category;
use App\Http\Resources\Article as ArticleResource;
use App\Http\Resources\ArticleCategory as ArticleCategoryResource;

class ArticleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::with(['articles' => function ($query) {
            $query->limit(4);
        }])->paginate();

        return ArticleCategoryResource::collection($categories);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function article(Article $article)
    {
        return new ArticleResource($article);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Article  $article
     * @return \Illuminate\Http\Response
     */
    public function category(Category $category)
    {
        $articles = $category->articles()->paginate(5);
		
        return ArticleResource::collection($articles);
    }

}

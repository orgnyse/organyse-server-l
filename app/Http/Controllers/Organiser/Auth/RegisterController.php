<?php

namespace App\Http\Controllers\Organiser\Auth;

use App\Models\Organiser;
use App\Models\Organisation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use App\Http\Resources\Organiser as OrganiserResource;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:api-organiser');
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        $organiser = $this->create($request->all());

        //event(new Registered($organiser));

        $token = $organiser->createToken('Organiser Token')->accessToken;

        return (new OrganiserResource($organiser))
            ->additional(['meta' => [
                'access_token' => $token 
            ]])->response()
            ->setStatusCode(201);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'firstname' => ['required', 'string', 'max:255'],
            'lastname' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:organisations'],
            'password' => ['required', 'string', 'min:8'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $organisation = Organisation::create([
            'name' => $data['name'],
            'email' => $data['email'],
        ]);

        $organiser = Organiser::withTrashed()->where('email', $data['email'])
            ->first();

        if (!$organiser) {

            $organiser = new Organiser;

            $organiser->fill([
                'firstname' => $data['firstname'],
                'lastname' => $data['lastname'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
            ]);

            $organiser->current_organisation_id = $organisation->id;

            $organiser->save();
            
        }

        if ($organiser->trashed()) {
            $organiser->restore();
        }

        $organisation->organisers()->syncWithoutDetaching($organiser);

        return $organiser;
        
    }

}

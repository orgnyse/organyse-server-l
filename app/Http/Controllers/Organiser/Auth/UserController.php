<?php

namespace App\Http\Controllers\Organiser\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\Task as TaskResource;
use App\Http\Resources\Organiser as OrganiserResource;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api-organiser');
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        return new OrganiserResource(Auth::guard('api-organiser')->user());
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function task()
    {
        $tasks = Auth::guard('api-organiser')->user()
            ->tasks()
            ->paginate(10);

        return TaskResource::collection($tasks);
    }


}

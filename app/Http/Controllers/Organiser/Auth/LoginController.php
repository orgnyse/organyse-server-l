<?php

namespace App\Http\Controllers\Organiser\Auth;

use App\Models\Organiser;
use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Validation\ValidationException;
use App\Http\Resources\Organiser as OrganiserResource;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:api-organiser')->except('logout');
    }

    /**
     * Handle a login request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);

        if (Auth::guard('web-organiser')->attempt($request->only('email', 'password'))) {
            
            $organiser = Organiser::where('email', $request->input('email'))->first();

            $organiser->tokens()
                ->delete();

            $token = $organiser->createToken('Organiser Token')
                ->accessToken;

            return (new OrganiserResource($organiser))
                    ->additional(['meta' => [
                        'access_token' => $token 
                    ]]);
        }

        throw ValidationException::withMessages([
            'email' => [trans('auth.failed')],
        ]);
    }

    /**
     * Validate the user login request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return void
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    protected function validateLogin(Request $request)
    {
        $request->validate([
            'email' => 'required|string',
            'password' => 'required|string',
        ]);
    }

    /**
     * Log the user out of the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::guard('api-organiser')->user()
			->tokens()
            ->delete();
            
        return new JsonResponse([
            'message' => 'You have been successfully logged out!'
        ], 200);

    }
}

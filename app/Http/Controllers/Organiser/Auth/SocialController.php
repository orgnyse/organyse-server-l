<?php

namespace App\Http\Controllers\Organiser\Auth;

use Illuminate\Http\Request;
use Laravel\Passport\Client;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Http;

class SocialController extends Controller
{
    /**
     * Handle a social authentication request to the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Http\JsonResponse
     *
     * @throws \Illuminate\Validation\ValidationException
     */
    public function social(Request $request)
    {
        $client = Client::where([
			'provider' => 'organisers', 
			'password_client' => 1
		])->first();

        $response = Http::post(route('passport.token'), [
            'grant_type' => 'social',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'provider' => $request->provider,
            'access_token' => $request->access_token
        ]);

        if ($response->ok()) {
            return new JsonResponse([
                'data' => $response->json()
            ], JsonResponse::HTTP_OK);
        }

        return new JsonResponse([
           $response->throw()->json()
        ], $response->status());
 
    }
}

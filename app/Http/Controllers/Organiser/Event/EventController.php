<?php

namespace App\Http\Controllers\Organiser\Event;

use Carbon\Carbon;
use App\Models\Event;
use App\Models\EventType;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Mews\Purifier\Facades\Purifier;
use App\Http\Controllers\Controller;
use App\Http\Resources\Event as EventResource;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api-organiser');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::with([
            'tickets'
        ])->paginate();

        return EventResource::collection($events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event;

        $event->fill($request->only([
            'title',
            'summary',
            'content'            
        ]));
		
		foreach ([
            'start_date',
            'end_date',
        ] as $date) {
            if ($request->get($date)) {
                $event->$date = Carbon::createFromFormat('d-m-Y H:i:s', $request->input($date))->toDateTimeString();
            }
        }

        $type = EventType::find($request->input('type'));

        $event->type()->associate($type);

        $event->save();
        
        $event->categories()
            ->attach($request->input('categories', []));

        if ($request->hasFile('flyer')) {
            $event->addMedia($request->file('flyer'))
                ->toMediaCollection('flyer');
        }

        return new JsonResponse([
            'message' => 'Event Created Successfully!'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function show(Event $event)
    {
        $event->loadMissing([
            'type',
            'media'
        ]);

        return new EventResource($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Event $event)
    {
        $event->fill($request->only([
            'title',
            'summary',
            'content'
        ]));

        //$event->content = Purifier::clean($request->input('content'));
		
		foreach ([
            'start_date',
            'end_date',
        ] as $date) {
            if ($request->get($date)) {
                $event->$date = Carbon::createFromFormat('d-m-Y H:i:s', $request->input($date))->toDateTimeString();
            }
        }

        $type = EventType::find($request->input('type'));

        $event->type()->associate($type);

        $event->save();

        $event->categories()
            ->sync($request->input('categories', []));

        if ($request->hasFile('flyer')) {
            $event->clearMediaCollection('flyer')
                ->addMedia($request->file('flyer'))
                ->toMediaCollection('flyer');
        }

        return new JsonResponse([
            'message' => 'Event Updated Successfully!'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Event  $event
     * @return \Illuminate\Http\Response
     */
    public function destroy(Event $event): JsonResponse
    {
        $event->delete();

        return new JsonResponse([
            'message' => 'Event has been successfully deleted'
        ]);
    }
}

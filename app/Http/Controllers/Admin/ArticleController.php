<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Article as ArticleResource;
use App\Http\Resources\ArticleCategory as ArticleCategoryResource;
use App\Models\Article;
use App\Models\ArticleCategory;
use DataTables;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mews\Purifier\Facades\Purifier;

class ArticleController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order = $request->input('order');
        $columns = $request->input('columns');
        $search = $request->input('search');
        if (isset($search) && ! empty($search['value'])) {
            $like_cols = [];
            foreach ($columns as $cols) {
                $like_cols[] = $cols['data'];
            }
            $query = Article::query();
            for ($count = 0; $count <= count($like_cols) - 1; $count ++) {
                if ($count == 0) {
                    $query->where($like_cols[$count], 'like', '%' . $search['value'] . '%');
                } else {
                    $query->orWhere($like_cols[$count], 'like', '%' . $search['value'] . '%');
                }
            }
            $articleData = $query->get();
        } elseif (isset($order[0]['column']) && $order[0]['column'] > 0) {
            $column_id = $order[0]['column'];
            $column_order_by = $order[0]['dir'];
            $column_name = $columns[$column_id]['data'];
            $articleData = Article::orderBy($column_name, $column_order_by)->get();
        } else {
            $articleData = Article::latest()->get();
        }
        return Datatables::of($articleData)->addIndexColumn()
            ->rawColumns([
            'action'
        ])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request, Article $article)
    {
        $request->validate([
            'title'   => 'required|string',
            'summary' => 'required|string',
            'content' => 'required|string',
        ]);

        $article->create([
            'user_id' => Auth::id(),
            'title'   => $request->input('title'),
            'summary' => Purifier::clean($request->input('summary')),
            'content' => Purifier::clean($request->input('content')),
        ]);

        return new JsonResponse([
            'message' => 'Article created successfully.',
        ], JsonResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param Article $article
     * @return \App\Http\Resources\Article
     */
    public function show(Article $article)
    {
        return new ArticleResource($article);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Article $article
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, Article $article)
    {
        $request->validate([
            'title'   => 'required|string',
            'summary' => 'required|string',
            'content' => 'required|string',
        ]);

        $article->update([
            'title'   => $request->input('title'),
            'summary' => Purifier::clean($request->input('summary')),
            'content' => Purifier::clean($request->input('content')),
        ]);

        return new JsonResponse([
            'message' => 'Article updated successfully.',
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Article $article
     * @return JsonResponse
     */
    public function destroy(Article $article): JsonResponse
    {
        $article->delete();

        return new JsonResponse([
            'message' => 'FAQ deleted successfully.',
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Display a listing of the categories of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories()
    {
        return ArticleCategoryResource::collection(ArticleCategory::paginate(20));
    }
}

<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

class ResetPasswordController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api')->except('postReset');
    }

    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
     */

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function reset(Request $request)
    {
        $request->validate($this->rules(), $this->validationErrorMessages());

        // Here we will attempt to reset the user's password. If it is successful we
        // will update the password on an actual user model and persist it to the
        // database. Otherwise we will parse the error and return the response.
        $response = $this->broker()->reset(
            $this->credentials($request), function ($user, $password) {
                $this->resetPassword($user, $password);
            }
        );

        // If the password was successfully reset, we will redirect the user back to
        // the application's home authenticated view. If there is an error we can
        // redirect them back to where they came from with their error message.
        return $response == Password::PASSWORD_RESET
        ? $this->sendResetResponse($request, $response)
        : $this->sendResetFailedResponse($request, $response);
    }

    /**
     * Get the password reset validation rules.
     *
     * @return array
     */
    protected function rules()
    {
        return [
            'token'    => 'required',
            'email'    => 'required|email',
            'password' => 'required|min:8',
        ];
    }

    /**
     * Get the password reset validation error messages.
     *
     * @return array
     */
    protected function validationErrorMessages()
    {
        return [];
    }

    /**
     * Get the password reset credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }

    /**
     * Reset the given user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function resetPassword($user, $password)
    {
        $this->setUserPassword($user, $password);

        $user->setRememberToken(Str::random(60));

        $user->save();

        event(new PasswordReset($user));
    }

    /**
     * Set the user's password.
     *
     * @param  \Illuminate\Contracts\Auth\CanResetPassword  $user
     * @param  string  $password
     * @return void
     */
    protected function setUserPassword($user, $password)
    {
        $user->password = Hash::make($password);
    }

    /**
     * Get the response for a successful password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetResponse(Request $request, $response)
    {
        return new JsonResponse([
            'message' => trans($response),
        ], 200);
    }

    /**
     * Get the response for a failed password reset.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $response
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    protected function sendResetFailedResponse(Request $request, $response)
    {
        throw ValidationException::withMessages([
            'email' => [trans($response)],
        ]);
    }

    /**
     * Get the broker to be used during password reset.
     *
     * @return \Illuminate\Contracts\Auth\PasswordBroker
     */
    public function broker()
    {
        return Password::broker('users');
    }

    /**
     * Reset Password For the user.
     * Old password / new password are the param
     * old_password / password API Params
     */

    public function resetPasswordApi(Request $request)
    {
        $inputs = [
            'old_password' => $request->input('old_password'),
            'password' => $request->input('password')
        ];
        $rules = [
            'old_password' => 'required',
            'password' => [
                'required',
                'min:6'
            ]
        ];
        $validation = \Validator::make($inputs, $rules);
        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validation->errors()
                    ->all()
            ]);
        }
        $usePassword = auth()->user()->password;
        $oldPassword = $request->input('old_password');
        if (! Hash::check($oldPassword, $usePassword)) {
            return response()->json([
                'success' => false,
                'message' => 'Old password donot match.'
            ]);
        } else {
            $newPassword = Hash::make($request->input('password'));
            $arrData = [
                'password' => $newPassword
            ];
            $queryResponse = DB::table('users')->where('id', auth()->user()->id)
                ->update($arrData);
            if ($queryResponse == 1) {
                return response()->json([
                    'success' => true,
                    'message' => 'Password updated successfully.'
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Something went wrong.Please try again later.'
                ]);
            }
        }
    }

    public function postReset(Request $request)
    {
        $inputs = [
            'password' => $request->input('password'),
            'token' => $request->input('token')
        ];
        $rules = [
            'password' => 'required',
            'token' => 'required'
        ];
        $validation = \Validator::make($inputs, $rules);
        if ($validation->fails()) {
            return response()->json([
                'success' => false,
                'message' => $validation->errors()
                    ->all()
            ]);
        }
        $password = Hash::make($request->password);
        $tokenData = DB::table('password_resets')->where('token', $request->token)->first();
        if (! $tokenData) {
            return response()->json([
                'success' => false,
                'message' => 'Token mismatch.'
            ]);
        }
        $user = DB::table('users')->where('email', $tokenData->email)->first();
        if (! $user) {
            return response()->json([
                'success' => false,
                'message' => 'User not found.'
            ]);
        } else {
            $arrData = [
                'password' => $password
            ];
            $queryResponse = DB::table('users')->where('id', $user->id)->update($arrData);
            if ($queryResponse == 1) {
                DB::table('password_resets')->where('email', $user->email)->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'Password updated successfully.'
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Something went wrong.Please try again later.'
                ]);
            }
        }
    }
}

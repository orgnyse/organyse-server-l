<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Organisation as OrganisationResource;
use App\Models\Organisation;

class OrganisationController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $order = $request->input('order');
        $columns = $request->input('columns');
        $search = $request->input('search');
        if (isset($search) && ! empty($search['value'])) {
            $like_cols = [];
            foreach ($columns as $cols) {
                $like_cols[] = $cols['data'];
            }
            $query = Organisation::query();
            for ($count = 0; $count <= count($like_cols) - 1; $count ++) {
                if ($count == 0) {
                    $query->where($like_cols[$count], 'like', '%' . $search['value'] . '%');
                } else {
                    $query->orWhere($like_cols[$count], 'like', '%' . $search['value'] . '%');
                }
            }
            $organisationData = $query->get();
        } elseif (isset($order[0]['column']) && $order[0]['column'] > 0) {
            $column_id = $order[0]['column'];
            $column_order_by = $order[0]['dir'];
            $column_name = $columns[$column_id]['data'];
            $organisationData = Organisation::orderBy($column_name, $column_order_by)->get();
        } else {
            $organisationData = Organisation::latest()->get();
        }
        return Datatables::of($organisationData)->addIndexColumn()
            ->rawColumns([
            'action'
        ])
            ->make(true);
    }
}


<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\Event as EventResource;
use App\Models\Event;
use App\Http\Resources\EventCategory as EventCategoryResource;
use App\Models\EventCategory;
use Illuminate\Http\Request;
use DataTables;

class EventController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return EventResource::collection(Event::paginate(20));
    }
    
    /**
     * Display a listing of the categories of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories(Request $request)
    {
        $order = $request->input('order');
        $columns = $request->input('columns');
        $search = $request->input('search');
        if (isset($search) && ! empty($search['value'])) {
            $like_cols = [];
            foreach ($columns as $cols) {
                $like_cols[] = $cols['data'];
            }
            $query = EventCategory::query();
            for ($count = 0; $count <= count($like_cols) - 1; $count ++) {
                if ($count == 0) {
                    $query->where($like_cols[$count], 'like', '%' . $search['value'] . '%');
                } else {
                    $query->orWhere($like_cols[$count], 'like', '%' . $search['value'] . '%');
                }
            }
            $eventCatData = $query->get();
        } elseif (isset($order[0]['column']) && $order[0]['column'] > 0) {
            $column_id = $order[0]['column'];
            $column_order_by = $order[0]['dir'];
            $column_name = $columns[$column_id]['data'];
            $eventCatData = EventCategory::orderBy($column_name, $column_order_by)->get();
        } else {
            $eventCatData = EventCategory::latest()->get();
        }
        return Datatables::of($eventCatData)->addIndexColumn()
            ->rawColumns([
            'action'
        ])
            ->make(true);
    }
}


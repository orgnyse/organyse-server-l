<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Mews\Purifier\Facades\Purifier;
use App\Http\Controllers\Controller;
use App\Http\Resources\Faq as FaqResource;
use Illuminate\Support\Facades\Auth;
use DataTables;

class FaqController extends Controller
{
	/**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
	
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $order = $request->input('order');
        $columns = $request->input('columns');
        $search = $request->input('search');
        if (isset($search) && ! empty($search['value'])) {
            $like_cols = [];
            foreach ($columns as $cols) {
                $like_cols[] = $cols['data'];
            }
            $query = Faq::query();
            for ($count = 0; $count <= count($like_cols) - 1; $count ++) {
                if ($count == 0) {
                    $query->where($like_cols[$count], 'like', '%' . $search['value'] . '%');
                } else {
                    $query->orWhere($like_cols[$count], 'like', '%' . $search['value'] . '%');
                }
            }
            $faqData = $query->get();
        } elseif (isset($order[0]['column']) && $order[0]['column'] > 0) {
            $column_id = $order[0]['column'];
            $column_order_by = $order[0]['dir'];
            $column_name = $columns[$column_id]['data'];
            $faqData = Faq::orderBy($column_name, $column_order_by)->get();
        } else {
            $faqData = Faq::latest()->get();
        }
        return Datatables::of($faqData)->addIndexColumn()
            ->rawColumns([
            'action'
        ])
            ->make(true);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
        ]);
        
        Auth::user()->faqs()->create([
			'title' => $request->input('title'),
			'content' => Purifier::clean($request->input('content')),
        ]);
        
        return new JsonResponse([
            'message' => 'FAQ created successfully.'
        ], JsonResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function show(Faq $faq)
    {
        return new FaqResource($faq);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
        ]);
        
        $faq->update([
			'title' => $request->input('title'),
			'content' => Purifier::clean($request->input('content')),
        ]);
        
        return new JsonResponse([
            'message' => 'FAQ updated successfully.'
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Faq  $faq
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq): JsonResponse
    {
        $faq->delete();

        return new JsonResponse([
            'message' => 'FAQ deleted successfully.'
        ], JsonResponse::HTTP_OK);
    }
}

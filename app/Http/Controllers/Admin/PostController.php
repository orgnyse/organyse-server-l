<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Mews\Purifier\Facades\Purifier;
use App\Http\Controllers\Controller;
use App\Http\Resources\Post as PostResource;
use Illuminate\Support\Facades\Auth;
use App\Http\Resources\PostCategory as PostCategoryResource;
use App\Models\PostCategory;

class PostController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::with([
            'categories',
            'media',
            'author',
            'author.media'
        ])->paginate();

        return PostResource::collection($posts);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
            'summary' => 'required|string',
        ]);
        
        $post = Post::create([
            'user_id' => Auth::id(),
            'title' => $request->input('title'),
            'content' => Purifier::clean($request->input('content')),
            'summary' => Purifier::clean($request->input('summary'))
        ]);

        $post->categories()->attach($request->input('categories', []));

        if ($request->hasFile('image')) {
            $post->addMedia($request->file('image'))
                ->toMediaCollection('featured');
        }

        return new JsonResponse([
            'message' => 'Post created successfully.'
        ], JsonResponse::HTTP_CREATED);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function show(Post $post)
    {
        $post->loadMissing([
            'categories',
            'media',
            'author',
            'author.media'
        ]);

        return new PostResource($post);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Post $post)
    {
        $request->validate([
            'title' => 'required|string',
            'content' => 'required|string',
            'summary' => 'required|string'
        ]);
        
        $post->update([
            'title' => $request->input('title'),
            'content' => Purifier::clean($request->input('content')),
            'summary' => Purifier::clean($request->input('summary'))
        ]);

        $post->categories()->sync($request->input('categories', []));

        if ($request->hasFile('image')) {
            $post->clearMediaCollection('featured')
                ->addMedia($request->file('image'))
                ->toMediaCollection('featured');
        }

        return new JsonResponse([
            'message' => 'Post updated successfully.'
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Post  $post
     * @return \Illuminate\Http\Response
     */
    public function destroy(Post $post): JsonResponse
    {
        $post->delete();
        
        return new JsonResponse([
            'message' => 'Post deleted successfully.'
        ], JsonResponse::HTTP_OK);
    }

    /**
     * Display a listing of the categories of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function categories()
    {
        return PostCategoryResource::collection(PostCategory::paginate(20));
    }
}

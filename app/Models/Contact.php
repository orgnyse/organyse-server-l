<?php

namespace App\Models;

use App\Traits\HasOrganisation;
use Kalnoy\Nestedset\NodeTrait;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Contact extends Model
{
    use HasOrganisation;
    use NodeTrait;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contacts';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname',
        'lastname',
        'email',
        'phone',
    ];

    /**
     * The categories associated with this contact.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\ContactCategory',
            'contact_category_contact', 
            'contact_id',
            'contact_category_id'
        );
    }
	
	/**
     * The groups associated with this contact.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function groups(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\ContactGroup',
            'contact_group_contact', 
            'contact_id',
            'contact_group_id'
        );
    }

}

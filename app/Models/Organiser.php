<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Glorand\Model\Settings\Traits\HasSettingsTable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Organiser extends Authenticatable implements HasMedia, MustVerifyEmail
{
    use Notifiable;
    use InteractsWithMedia;
    use HasApiTokens;
    use HasRoles;
    use HasSettingsTable;
    use SoftDeletes;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'organisers';

    /**
     * The guard associated with the model.
     *
     * @var string
     */
    protected $guard_name = 'api-organiser';

	/**
     * The relationships that should always be loaded.
     *
     * @var array
     */
    protected $with = [
		'organisations'
	];
	
	/**
     * Default settings of this model.
     *
     * @var array
     */
    public $defaultSettings = [
        'security' => [
            '2fa' => false
        ],
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'firstname', 
        'lastname', 
        'email', 
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Get the organisations associated witth this organiser.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organisations(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Organisation',
            'organisation_organiser',
            'organisation_id',
            'organiser_id'
        );
    }

    /**
     * Get the tasks associated witth this organiser.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tasks(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Task',
            'organiser_task',
            'organiser_id',
            'task_id'
        );
    }
	
	/**
     * A model may have multiple roles.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles(): BelongsToMany
    {
        return $this->morphToMany(
            config('permission.models.role'),
            'model',
            config('permission.table_names.model_has_roles'),
            config('permission.column_names.model_morph_key'),
            'role_id'
        )->where('organisation_id', auth()->guard('api-organiser')->user()->current_organisation_id);
    }
	
	/**
     * A model may have multiple direct permissions.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions(): BelongsToMany
    {
        return $this->morphToMany(
            config('permission.models.permission'),
            'model',
            config('permission.table_names.model_has_permissions'),
            config('permission.column_names.model_morph_key'),
            'permission_id'
        )->where('organisation_id', auth()->guard('api-organiser')->user()->current_organisation_id);
    }

}

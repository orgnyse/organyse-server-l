<?php

namespace App\Models;

use App\Traits\HasOrganisation;
use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Task extends Model implements HasMedia
{
    use InteractsWithMedia;
    use HasOrganisation;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'tasks';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'subject',
        'description'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'date',
        'due_date' => 'date'
    ];

    /**
     * The assignees this task has been assigned to.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function assignees(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Organiser',
            'organiser_task',
            'task_id',
            'organiser_id'
        );
    }

    /**
     * Get the comments associated with this task.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments(): HasMany 
    {
        return $this->hasMany(
            'App\Models\TaskComment',
            'task_id',
            'id'
        );
    }
}

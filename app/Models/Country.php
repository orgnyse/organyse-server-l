<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Country extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'countries';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'capital',
        'iso_alpha2',
        'iso_alpha3',
        'iso_numeric',
        'address_format',
        'is_eu_member',
        'uses_postal_code',
        'emoji'
    ];

    /**
     * The currencies used by this country.
     */
    public function currencies(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Currency', 
            'country_currency', 
            'country_id', 
            'currency_id'
        );
    }
	
    /**
     * Get the zones for the country.
     */
    public function states(): HasMany
    {
        return $this->hasMany(
            'App\Models\Division', 
            'country_id', 
            'id'
        );
    }
}

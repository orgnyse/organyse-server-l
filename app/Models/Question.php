<?php

namespace App\Models;

use App\Traits\HasOrganisation;
use Spatie\EloquentSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\SortableTrait;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Question extends Model implements Sortable
{
    use SortableTrait;
    use HasOrganisation;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 
        'description',
        'values', 
        'default_value',
        'placeholder',
        'required',
        'system_default'
    ];
	
	/**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'values' => 'array',
        'required' => 'integer'
    ];

    /**
     * Encode question values to json.
     *
     * @param  string  $value
     * @return void
     */
    public function setValueAttribute($value): void
    {
        $this->attributes['value'] = json_encode((array)$value);
    }

    /**
     * Get the type of the question.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(
			'App\Models\QuestionType',
			'question_type_id',
			'id'
		)->withDefault();
    }

    /**
     * The events associated with the question
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function events(): BelongsToMany
    {
        return $this->belongsToMany(
			'App\Models\Event',
			'event_question',
			'question_id',
			'event_id'
		);
    }

    /**
     * The tickets associated with the question
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tickets(): BelongsToMany
    {
        return $this->belongsToMany(
			'App\Models\Ticket',
			'ticket_question',
			'question_id',
			'ticket_id'
		);
    }
}

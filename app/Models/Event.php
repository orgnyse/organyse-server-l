<?php

namespace App\Models;

use Spatie\Sluggable\HasSlug;
use App\Traits\HasOrganisation;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\Sluggable\SlugOptions;
use Spatie\EloquentSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Spatie\EloquentSortable\SortableTrait;
use Spatie\Activitylog\Traits\LogsActivity;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Event extends Model implements HasMedia, Sortable
{
    use InteractsWithMedia;
    use HasSlug;
    use SortableTrait;
    use NodeTrait;
    use LogsActivity;
    use HasOrganisation;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 
        'summary', 
        'content'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'start_date' => 'datetime',
        'end_date' => 'datetime',
    ];

    /**
     * The type associated with this event.
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function type(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\EventType',
            'event_type_id',
            'id'
        )->withDefault();
    }

    /**
     * The categories that belongs to the event.
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function categories(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\EventCategory',
            'event_category_event',
            'event_id',
            'event_category_id'
        );
    }

    /**
     * The tags associated with this event.
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function tags(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\EventTag',
            'event_tag_event',
            'event_id',
            'event_tag_id'
        );
    }

    /**
     * The venue for this event.
     * 
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function venue(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Venue',
            'venue_id',
            'id'
        )->withDefault();
    }

    /**
     * The tickets for this event.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tickets(): HasMany
    {
        return $this->hasMany(
            'App\Models\Ticket',
            'event_id',
            'id'
        );
    }

    /**
     * The design options for this event.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function design(): HasOne
    {
        return $this->hasOne(
            'App\Models\EventDesign',
            'event_id', 
            'id'
        )->withDefault([
            'show_email' => 0,
            'email' => 'abc@xyz.com',
            'show_phone' => 0,
            'phone' => '0140000',
            'bg_color' => '#d94056',
            'image_position' => 'Above the content',
            'ticket_section_name' => '',
            'ticket_section_desc' => '',
            'reg_button_text' => '',
            'checkout_button_text' => '',
            'offline_method_text' => ''
        ]);
    }

    /**
     * The questions associated with the question
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function questions(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Question',
            'event_question',
            'event_id',
            'question_id',
        )->ignoreOrganisation();
    }

    /**
     * The sponsors for this event.
     * 
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sponsors(): HasMany
    {
        return $this->hasMany(
            'App\Models\EventSponsor',
            'event_id',
            'id'
        );
    }

    /**
     * Get the options for generating the slug.
     * 
     * @return \Spatie\Sluggable\SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()->generateSlugsFrom('title')
            ->saveSlugsTo('slug');
    }

    /**
     * Define media conversions
     * 
     * @param \Spatie\MediaLibrary\Models\Media
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
        ->fit(Manipulations::FIT_CROP, 368, 232)
            ->performOnCollections('featured');
    }

    /**
     * Scope a query to only include published events.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopePubished($query): Builder
    {
        return $query->where([
            'is_published' => 1,
        ]);
    }
}

<?php

namespace App\Models;

use Spatie\EloquentSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\SortableTrait;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SponsorLevel extends Model implements Sortable
{
    use SortableTrait;
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sponsor_levels';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'order_column'
    ];

    /**
     * Get the sponsors associated with this level.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function organisation(): HasMany
    {
        return $this->hasMany(
            'App\Models\Sponsor',
            'sponsor_level_id',
            'id'
        );
    }


}

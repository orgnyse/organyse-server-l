<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class Seo extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seos';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'meta',
        'og',
        'twitter',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'meta' => 'array',
        'og' => 'array',
        'twitter' => 'array',
    ];

    /**
     * Get the owning seoable model.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function seoable() : MorphTo
    {
        return $this->morphTo();
    }


}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EventSponsor extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_sponsors';

    /**
     * Get the event associated with this model.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Event',
            'event_id',
            'id'
        )->withDefault();
    }

    /**
     * Get the sponsor associated with this model.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sponsor(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Sponsor',
            'sponsor_id',
            'id'
        )->withDefault();
    }

    /**
     * Get the level of the sponsor associated with the model.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function level(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\SponsorLevel',
            'sponsor_level_id',
            'id'
        )->withDefault();
    }


}

<?php

namespace App\Models;

use Spatie\Sluggable\HasSlug;
use App\Traits\HasOrganisation;
use Kalnoy\Nestedset\NodeTrait;
use Spatie\Sluggable\SlugOptions;
use Spatie\EloquentSortable\Sortable;
use Illuminate\Database\Eloquent\Model;
use Spatie\EloquentSortable\SortableTrait;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class ContactCategory extends Model implements Sortable
{
    use HasSlug;
    use SortableTrait;
    use NodeTrait;
    use HasOrganisation;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contact_categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title',
        'description'
    ];

    /**
     * The contacts that belong to the category.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contacts(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Contact',
            'contact_category_contact',
            'contact_category_id',
            'contact_id'
        );
    }

    /**
     * Get the options for generating the slug.
     * 
     * @return \Spatie\Sluggable\SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()->generateSlugsFrom('title')
        ->saveSlugsTo('slug');
    }

}

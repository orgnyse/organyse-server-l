<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class EventDesign extends Model implements HasMedia
{
    use InteractsWithMedia;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_designs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'show_email',
        'email',
        'show_phone',
        'phone',
        'bg_color',
        'image_position',
        'ticket_section_name',
        'ticket_section_desc',
        'reg_button_text',
        'checkout_button_text',
        'offline_method_text'
    ];

    /**
     * The event associated with this design
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function event(): BelongsTo
    {
        return $this->belongTo(
            'App\Models\Event',
            'event_id',
            'id'
        )->withDefault();
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class EventType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'event_types';
}

<?php

namespace App\Models;

use Spatie\Sluggable\HasSlug;
use Spatie\Image\Manipulations;
use Spatie\MediaLibrary\HasMedia;
use Spatie\Sluggable\SlugOptions;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Organisation extends Model implements HasMedia
{
    use InteractsWithMedia;
    use SoftDeletes;
    use HasSlug;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'organisations';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'bio',
        'email',
        'address1',
        'address2',
        'suburb',
        'postcode'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'deleted_at' => 'datetime',
    ];

    /**
     * Get the events associated witth this orgnisation.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function events(): HasMany
    {
        return $this->hasMany(
            'App\Models\Event',
            'organisation_id',
            'id'
        )->ignoreOrganisation();
    }

    /**
     * Get the state associated witth this orgnisation.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function state(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Division',
            'division_id',
            'id'
        )->withDefault();
    }

    /**
     * Get the country associated witth this orgnisation.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country(): BelongsTo
    {
        return $this->belongsTo(
            'App\Models\Country',
            'country_id',
            'id'
        )->withDefault();
    }

    /**
     * Get the organisers associated witth this orgnisation.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function organisers(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Organiser',
            'organisation_organiser',
            'organisation_id',
            'organiser_id',
        );
    }

    /**
     * The contacts associated with this contact.
     * 
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function contacts(): BelongsToMany
    {
        return $this->belongsToMany(
            'App\Models\Contact',
            'contact_organisation',
            'contact_id',
            'organisation_id'
        );
    }

    /**
     * Get the options for generating the slug.
     * 
     * @retutn \Spatie\Sluggable\SlugOptions
     */
    public function getSlugOptions(): SlugOptions
    {
        return SlugOptions::create()
            ->generateSlugsFrom('name')
            ->saveSlugsTo('slug');
    }

    /**
     * Define media conversions
     * 
     * @param \Spatie\MediaLibrary\Models\Media
     * @return void
     */
    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')
            ->fit(Manipulations::FIT_CROP, 30, 30)
            ->performOnCollections('featured');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuestionType extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'question_types';
}

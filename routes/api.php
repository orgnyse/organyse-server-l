<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Admin\BackgroundController as AdminBackground;
use App\Http\Controllers\Admin\Auth\ForgotPasswordController as AdminForgotPassword;
use App\Http\Controllers\Admin\Auth\ResetPasswordController as AdminResetPassword;
use App\Http\Controllers\Admin\Auth\LoginController as AdminLogin;
use App\Http\Controllers\Admin\FaqController as AdminFaq;
use App\Http\Controllers\Admin\ArticleController as AdminArticle;
use App\Http\Controllers\Admin\EventController as AdminEvent;
use App\Http\Controllers\Admin\OrganisationController as AdminOrganisation;
use App\Http\Controllers\Admin\PostController as AdminPost;

use App\Http\Controllers\Organiser\Contact\CategoryController as OrganiserContactCategory;
use App\Http\Controllers\Organiser\Contact\ContactController as OrganiserContact;
use App\Http\Controllers\Organiser\Team\TaskController as OrganiserTask;
use App\Http\Controllers\Organiser\Team\MeetingController as OrganiserMeeting;
use App\Http\Controllers\Organiser\Event\SponsorController as OrganiserEventSponsor;
use App\Http\Controllers\Organiser\Event\DataController as OrganiserEventData;

use App\Http\Controllers\Organiser\Event\TicketController as OrganiserEventTicket;
use App\Http\Controllers\Organiser\Event\DesignController as OrganiserEventDesign;
use App\Http\Controllers\Organiser\Event\VenueController as OrganiserEventVenue;
use App\Http\Controllers\Organiser\Event\EventController as OrganiserEvent;
use App\Http\Controllers\Organiser\Sponsor\SponsorController as OrganiserSponsor;
use App\Http\Controllers\Organiser\Venue\VenueController as OrganiserVenue;
use App\Http\Controllers\Organiser\Survey\QuestionController as OrganiserQuestion;
use App\Http\Controllers\Organiser\Auth\VerificationController as OrganiserVerification;
use App\Http\Controllers\Organiser\Auth\ForgotPasswordController as OrganiserForgotPassword;
use App\Http\Controllers\Organiser\Auth\ResetPasswordController as OrganiserResetPassword;
use App\Http\Controllers\Organiser\Auth\RegisterController as OrganiserRegister;
use App\Http\Controllers\Organiser\Auth\LoginController as OrganiserLogin;
use App\Http\Controllers\Organiser\Auth\UserController as OrganiserUser;

use App\Http\Controllers\Portal\ContactController as PortalContact;
use App\Http\Controllers\Portal\FaqController as PortalFaq;
use App\Http\Controllers\Portal\PostController as PortalPost;
use App\Http\Controllers\Portal\ArticleController as PortalArticle;
use App\Http\Controllers\Portal\OrganisationController as PortalOrganisation;
use App\Http\Controllers\Portal\EventController as PortalEvent;

use App\Http\Controllers\Core\TimezoneController as CoreTimezone;
use App\Http\Controllers\Core\CountryController as CoreCountry;
use App\Http\Controllers\Core\CurrencyController as CoreCurrency;


Route::prefix('admin')->group(function () {

    Route::get('background', [AdminBackground::class, 'index']);
    Route::post('background', [AdminBackground::class, 'store']);
    Route::get('background/{background}', [AdminBackground::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'background/{background}', [AdminBackground::class, 'update']);
    Route::delete('background/{background}', [AdminBackground::class, 'destroy']);

    Route::post('login', [AdminLogin::class, 'login']);
    Route::post('logout', [AdminLogin::class, 'logout']);
    Route::post('password/reset', [AdminResetPassword::class, 'resetPasswordApi']);
    Route::post('password/email', [AdminForgotPassword::class, 'sendResetLinkPassword']);
    Route::post('password/recover', [AdminResetPassword::class, 'postReset']);
    
    Route::prefix('faq')->group(function() {
        Route::post('index', [AdminFaq::class, 'index']);
        Route::post('add', [AdminFaq::class, 'store']);
        Route::get('{faq}', [AdminFaq::class, 'show']);
        Route::match(['put', 'patch', 'post'], '{faq}', [AdminFaq::class, 'update']);
        Route::delete('{faq}', [AdminFaq::class, 'destroy']);
    });

    Route::prefix('article')->group(function () {
        Route::post('index', [AdminArticle::class, 'index']);
        Route::post('add', [AdminArticle::class, 'store']);
        Route::get('{article}', [AdminArticle::class, 'show']);
        Route::match(['put', 'patch', 'post'], '{article}', [AdminArticle::class, 'update']);
        Route::delete('{article}', [AdminArticle::class, 'destroy']);
    });
    Route::get('article-categories', [AdminArticle::class, 'categories']);
    
    Route::get('event', [AdminEvent::class, 'index']);
    Route::post('event-categories', [AdminEvent::class, 'categories']);
    
    Route::get('organisation', [AdminOrganisation::class, 'index']);
        
    Route::prefix('post')->group(function () {
        Route::post('index', [AdminPost::class, 'index']);
        Route::post('add', [AdminPost::class, 'store']);
        Route::get('{post}', [AdminPost::class, 'show']);
        Route::match(['put', 'patch', 'post'], '{post}', [AdminPost::class, 'update']);
        Route::delete('{post}', [AdminPost::class, 'destroy']);
    });
    Route::get('post-categories', [AdminPost::class, 'categories']);
});

Route::prefix('organiser')->group(function () {
    
    Route::get('contact/category', [OrganiserContactCategory::class, 'index']);
    Route::post('contact/category', [OrganiserContactCategory::class, 'store']);
    Route::get('contact/category/{category}', [OrganiserContactCategory::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'contact/category/{category}', [OrganiserContactCategory::class, 'update']);
    Route::delete('contact/category/{category}', [OrganiserContactCategory::class, 'destroy']);

    Route::get('contact', [OrganiserContact::class, 'index']);
    Route::post('contact', [OrganiserContact::class, 'store']);
    Route::get('contact/{contact}', [OrganiserContact::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'contact/{contact}', [OrganiserContact::class, 'update']);
    Route::delete('contact/{contact}', [OrganiserContact::class, 'destroy']);

    Route::get('task', [OrganiserTask::class, 'index']);
    Route::post('task', [OrganiserTask::class, 'store']);
    Route::get('task/{task}', [OrganiserTask::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'task/{task}', [OrganiserTask::class, 'update']);
    Route::delete('task/{task}', [OrganiserTask::class, 'destroy']);

    Route::get('meeting', [OrganiserMeeting::class, 'index']);
    Route::get('meeting/{note}', [OrganiserMeeting::class, 'show']);
    Route::post('meeting', [OrganiserMeeting::class, 'store']);
    Route::match(['put', 'patch', 'post'], 'meeting/{note}', [OrganiserMeeting::class, 'update']);
    Route::delete('meeting/{note}', [OrganiserMeeting::class, 'destroy']);

    Route::get('event/{event}/sponsor', [OrganiserEventSponsor::class, 'index']);
    Route::post('event/{event}/sponsor', [OrganiserEventSponsor::class, 'store']);
    Route::delete('event/{event}/sponsor/{sponsor}', [OrganiserEventSponsor::class, 'destroy']);

    Route::get('event/{event}/data', [OrganiserEventData::class, 'index']);
    Route::post('event/{event}/data', [OrganiserEventData::class, 'store']);

    Route::get('event/{event}/ticket', [OrganiserEventTicket::class, 'index']);
    Route::post('event/{event}/ticket', [OrganiserEventTicket::class, 'store']);
    Route::get('event/{event}/ticket/{ticket}', [OrganiserEventTicket::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'event/{event}/ticket/{ticket}', [OrganiserEventTicket::class, 'update']);
    Route::delete('event/{event}/ticket/{ticket}', [OrganiserEventTicket::class, 'destroy']);

    Route::get('event/{event}/design', [OrganiserEventDesign::class, 'index']);
    Route::post('event/{event}/design', [OrganiserEventDesign::class, 'store']);

    Route::get('event/{event}/venue', [OrganiserEventVenue::class, 'index']);
    Route::post('event/{event}/venue', [OrganiserEventVenue::class, 'store']);

    Route::get('event', [OrganiserEvent::class, 'index']);
    Route::post('event', [OrganiserEvent::class, 'store']);
    Route::get('event/{event}', [OrganiserEvent::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'event/{event}', [OrganiserEvent::class, 'update']);
    Route::delete('event/{event}', [OrganiserEvent::class, 'destroy']);

    Route::get('sponsor', [OrganiserSponsor::class, 'index']);
    Route::post('sponsor', [OrganiserSponsor::class, 'store']);
    Route::get('sponsor/{sponsor}', [OrganiserSponsor::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'sponsor/{sponsor}', [OrganiserSponsor::class, 'update']);
    Route::delete('sponsor/{sponsor}', [OrganiserSponsor::class, 'destroy']);

    Route::get('venue', [OrganiserVenue::class, 'index']);
    Route::post('venue', [OrganiserVenue::class, 'store']);
    Route::get('venue/{venue}', [OrganiserVenue::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'venue/{venue}', [OrganiserVenue::class, 'update']);
    Route::delete('venue/{venue}', [OrganiserVenue::class, 'destroy']);

    Route::get('question', [OrganiserQuestion::class, 'index']);
    Route::post('question', [OrganiserQuestion::class, 'store']);
    Route::get('question/{question}', [OrganiserQuestion::class, 'show']);
    Route::match(['put', 'patch', 'post'], 'question/{question}', [OrganiserQuestion::class, 'update']);
    Route::delete('question/{question}', [OrganiserQuestion::class, 'destroy']);

    Route::get('user/task', [OrganiserUser::class, 'task']);
    Route::get('user', [OrganiserUser::class, 'user']);

    Route::post('email/resend', [OrganiserVerification::class, 'resend']);
    Route::post('password/email', [OrganiserForgotPassword::class, 'sendResetLinkEmail']);
    Route::post('password/reset', [OrganiserResetPassword::class, 'reset']);
    Route::post('register', [OrganiserRegister::class, 'register']);
    Route::post('login', [OrganiserLogin::class, 'login']);
    Route::post('logout', [OrganiserLogin::class, 'logout']);

});

Route::post('contact', [PortalContact::class, 'handleResponse']);
Route::get('faqs', [PortalFaq::class, 'index']);
Route::get('articles', [PortalPost::class, 'index']);
Route::get('articles/{post:slug}', [PortalPost::class, 'post']);
Route::get('articles/category/{category:slug}', [PortalPost::class, 'category']);

Route::get('support-center', [PortalArticle::class, 'index']);
Route::get('support-center/{article:slug}', [PortalArticle::class, 'article']);
Route::get('support-center/category/{category:slug}', [PortalArticle::class, 'category']);

Route::get('organisation', [PortalOrganisation::class, 'index']);
Route::get('organisation/{organisation}', [PortalOrganisation::class, 'show']);
Route::get('organisation/{organisation}/event', [PortalOrganisation::class, 'event']);

Route::get('event', [PortalEvent::class, 'index']);
Route::get('event/{event}', [PortalEvent::class, 'show']);

Route::get('timezone', [CoreTimezone::class, 'index']);
Route::get('country/{country:iso_alpha3}', [CoreCountry::class, 'country']);
Route::get('country/{country:iso_alpha3}/zone', [CoreCountry::class, 'division']);
Route::get('country', [CoreCountry::class, 'index']);
Route::get('currency/{currency:iso_code}', [CoreCurrency::class, 'currency']);
Route::get('currency', [CoreCurrency::class, 'index']);